import os
import time
import numpy as np
import glob

from pyclick.click_models.CCM import CCM
from pyclick.click_models.DBN import DBN
from pyclick.click_models.task_centric.TaskCentricSearchSession import TaskCentricSearchSession
from pyclick.search_session.SearchResult import SearchResult


def binary_search(f, query):
    """
      Search query and JSON in file f.

      :returns: line with query and JSON or False.
      """
    r = os.fstat(f.fileno()).st_size
    l = 0
    x = int(query)

    while ((r - l) > 1):
        query = (r + l) >> 1

        f.seek(query)
        while f.read(1) != '\n':
            pass

        line = f.readline()
        data = line.split("\t")

        elem = int(data[0])

        if elem > x:
            r = query
        if elem < x:
            l = query

        if (elem == x):
            return line

    return False


def simulating_user_clicks(click_model, query_session):
    """
   Simulates search sessions.

   :returns: Vector of simulated clicks.
   """
    simulated_clicks = []

    session_len = len(query_session.web_results)
    for i in range(session_len):
        query_session.web_results[i].click = 0

    for i in np.arange(session_len):
        P_r = click_model.get_conditional_click_probs(query_session)
        p = P_r[i]
        simulated_click = np.random.binomial(1, p, 1)[0]
        if (simulated_click == 1):
            query_session.web_results[i].click = 1

        simulated_clicks.append(simulated_click)

    return simulated_clicks


class SessionsIterator(object):
    def __init__(self, fname, session_max=None):
        self.f = open(fname, 'r')
        self.session_max = session_max
        self.prev_line = 'start'
        self.flag = False
        self.line_number = 0

        while (self.line_number < -1):
            read_line = self.f.readline()
            self.line_number += 1

    def __iter__(self):
        return self

    def next(self):
        sessions = []

        while True:
            if (self.flag):
                line = self.prev_line
                self.flag = False
            else:
                line = self.f.readline()
                self.line_number += 1

                if (self.line_number % 100000 == 0):
                    print self.line_number

            entry_array = line.strip().split("\t")

            if len(entry_array) == 4 and entry_array[1] == 'M':
                pass

            elif len(entry_array) >= 7 and (entry_array[2] == 'Q' or entry_array[2] == 'T'):

                if self.session_max and len(sessions) >= self.session_max:
                    self.prev_line = line
                    break

                task = entry_array[0]
                serp = entry_array[3]
                query = entry_array[4]
                urls_domains = entry_array[6:]
                session = TaskCentricSearchSession(task, query)

                results = []
                for url_domain in urls_domains:
                    result = url_domain.strip().split(',')[0]
                    url_domain = SearchResult(result, 0)
                    results.append(result)

                    session.web_results.append(url_domain)

                sessions.append(session)

            elif len(entry_array) == 5 and entry_array[2] == 'C':
                if entry_array[0] == task and entry_array[3] == serp:
                    clicked_result = entry_array[4]
                    if clicked_result in results:
                        index = results.index(clicked_result)
                        session.web_results[index].click = 1
            else:
                if not line:
                    if (sessions != []):
                        self.flag = False
                        return sessions

                    raise StopIteration

        if (sessions != []):
            self.flag = True
            return sessions

        raise StopIteration


click_model = globals()["DBN"]()

sorted_model_file = open("/Users/Stepan/Desktop/models/DBN_sorted", 'r')

session_counter = 0
start = time.time()

list_of_names = glob.glob("/Users/Stepan/Desktop/test_data/real_test_days/*")
for name in list_of_names:
    print(name)
    day = name[-6:-4]
    real_sessions_iter = SessionsIterator(name, 100000)
    file_with_real_session = open(
            "/Users/Stepan/Desktop/test_data/real_test_JSON_days/real_test_JSON_day_" + day + ".txt", 'w')
    file_with_predicted_session = open(
            "/Users/Stepan/Desktop/test_data/DBN_pred_test_JSON_days/DBN_pred_test_JSON_day_" + day + ".txt", 'w')
    sessions = real_sessions_iter.next()
    for session in sessions:
        file_with_real_session.write(str(session_counter) + "\t" + str(session) + "\n")

        query = session.query
        line = binary_search(sorted_model_file, query)
        _, json_for_query_q = line.split("\t")
        click_model.from_json(json_for_query_q)
        predicted_clicks = simulating_user_clicks(click_model, session)

        session.change_clicks(predicted_clicks)

        file_with_predicted_session.write(str(session_counter) + "\t" + str(session) + "\n")

        session_counter += 1

    file_with_real_session.close()
    file_with_predicted_session.close()

sorted_model_file.close()
end = time.time()

print "Session counter is", session_counter
print "Work time is", (end - start) / 60, 'min'
