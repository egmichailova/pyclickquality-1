import os
import time

import numpy as np
from pyclick.click_models.UBM import UBM
from pyclick.click_models.DBN import DBN
from pyclick.click_models.SDBN import SDBN
from pyclick.click_models.DCM import DCM
from pyclick.click_models.CCM import CCM
from pyclick.click_models.CTR import DCTR, RCTR, GCTR
from pyclick.click_models.CM import CM
from pyclick.click_models.PBM import PBM
from pyclick.utils.YandexPersonalizedChallengeParser import YandexPersonalizedChallengeParser


def binary_search(f, query):
  """
    Search query and JSON in file f.

    :returns: line with query and JSON or False.
    """
  r = os.fstat(f.fileno()).st_size
  l = 0
  x = int(query)

  while r-l > 1:
      query = (r + l) >> 1

      f.seek(query)
      while f.read(1) != '\n':
          pass

      line = f.readline()
      data = line.split("\t")

      elem = int(data[0])

      if elem > x:
        r = query
      if elem < x:
        l = query

      if (elem == x):
        return line

  return False


def simulating_user_clicks(click_model, query_session):
     """
    Simulates search sessions.

    :returns: Vector of simulated clicks.
    """
     simulated_clicks = []

     P_r = click_model.get_conditional_click_probs(query_session)
     for p in P_r:
         simulated_clicks.append(np.random.binomial(1, p, 1)[0])

     return simulated_clicks

def mae_first_last_clicks(pred, real):
    """
    Mean Absolute Error of first and last real and predicted clicks.

    :returns: Two MAE.
    """
    flag_pred = True
    flag_real = True

    r_first_pred = None
    r_first_real = None
    r_last_pred = None
    r_last_real = None

    for i in xrange(len(pred)):
        if (flag_pred and pred[i] == 1):
            r_first_pred = i
            flag_pred = False
        if (flag_real and real[i] == 1):
            r_first_real = i
            flag_real = False

        if ((not flag_pred) and pred[i] == 1):
            r_last_pred = i
        if ((not flag_real) and real[i] == 1):
            r_last_real = i

    if (r_first_pred == None or r_first_real == None):
        return None

    return abs(r_first_pred - r_first_real), abs(r_last_pred - r_last_real)

def mean_squared_error(pred, real):
    """
    Mean Squared Error of real and predicted clicks.

    :returns: MSE.
    """
    pred = np.array(pred)
    real = np.array(real)

    return ((pred - real) ** 2).mean(axis=0)


click_model = globals()["CCM"]()
all_query_file = open("/Users/Stepan/Desktop/data/all_query_6e6-7e6.txt", 'r')
result_file = open("/Users/Stepan/Desktop/result/result_6e6-7e6.txt", 'w')
sorted_model_file = open("/Users/Stepan/Desktop/models/CCM_sorted", 'r')
did_not_find_file = open("/Users/Stepan/Desktop/did_not_find/did_not_find", 'w')

cc = 0
exp_cc = 0
str_exp = 0
io_exp = 0
zero_real_clicks_counter = 0
did_not_find = 0
start = time.time()

results = ''

for query in all_query_file:
    try:
        query = query.strip()
        try:
            search_sessions = YandexPersonalizedChallengeParser().parse("/Users/Stepan/Desktop/session_data_6e6-7e6/" + query)
        except IOError:
            print 'can\'t find file with name', query
            io_exp += 1
            continue
        try:
            line = binary_search(sorted_model_file, query)
            if (line == False):
                did_not_find_file.write("did not find query" +  query + '\n')
                did_not_find += 1
                continue


            _, json_for_query_q = line.split("\t")
        except Exception:
            exp_cc += 1
            continue

        click_model.from_json(json_for_query_q)

        for session in search_sessions:
            real_clicks = session.get_clicks()
            pred_clicks = simulating_user_clicks(click_model, session)
            # print query, real_clicks, pred_clicks

            mea = mae_first_last_clicks(pred_clicks, real_clicks)
            mse = mean_squared_error(pred_clicks, real_clicks)
            if (mea == None):
                zero_real_clicks_counter += 1

            results += query + '\t' + str(mea) + '\t' + str(mse) + '\n'

    except Exception:
        print "Strange exception"
        str_exp += 1

    cc += 1

    if (cc % 100000 == 0):
        print cc

result_file.write(results)
result_file.close()
all_query_file.close()

end = time.time()
print '\nresults:'
print "Time for", cc, "query is", (end - start) / 60, 'min'
print 'did_not_find', did_not_find, 'from', cc
print 'zero_real_clicks_counter', zero_real_clicks_counter
print 'exp_cc', exp_cc
print 'str_exp', str_exp
print 'io_exp', io_exp