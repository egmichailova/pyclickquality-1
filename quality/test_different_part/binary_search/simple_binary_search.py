def binary_search(a, x):
    l = 0
    r = len(a)

    while (r - l > 1):
        m = (r + l) >> 1

        if a[m] > x:
            r = m
        else:
            l = m

    if (a[l] != x):
        return -1
    else:
        return l



a = [1, 5, 10, 22, 30 ,100]

print(binary_search(a, 5))