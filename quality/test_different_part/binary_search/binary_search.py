import os

model_file = open("/Users/Stepan/Desktop/models/CCM.1-14")
sorted_model_file = open("/Users/Stepan/Desktop/models/CCM_sorted", 'r')

def binary_search(f, query):
  r = os.fstat(f.fileno()).st_size
  l = 0
  x = int(query)

  while r-l > 1:
      query = (r + l) >> 1

      f.seek(query)
      while f.read(1) != '\n':
          pass

      line = f.readline()
      data = line.split("\t")

      elem = int(data[0])

      if elem > x:
        r = query
      if elem < x:
        l = query

      if (elem == x):
        return line

  return False


all_query_sorted_model_file = open("/Users/Stepan/Desktop/data/all_query.txt", 'r')

f_cc = 0
c = 0
exp_c = 0
for query in all_query_sorted_model_file:
    try:
        res = binary_search(sorted_model_file, query)
    except Exception:
        print 'exp'
        exp_c += 1

    if  (res == False):
        print query
        f_cc += 1

    c += 1


print exp_c,  f_cc, c