import glob

list_of_file_path =  glob.glob("/Users/Stepan/Desktop/CCM_work/result/*.txt")


r_first_sum = float(0)
r_last_sum = float(0)
mse_sum = float(0)
cc = float(0)
all_cc = float(0)


list_r_first = []
for path in list_of_file_path:
    print path
    file = open(path, 'r')
    for line in file:
        line = line.strip()
        lines = line.split("\t")
        query, res, mse = lines

        if (res != 'None'):
            data = res.split(', ')
            r_first = (float)(data[0][1])
            list_r_first.append(r_first)

            r_last = (float)(data[1][0])

            r_first_sum += r_first
            r_last_sum += r_last

            cc += 1

        mse_sum += (float)(mse)
        all_cc += 1

print cc, all_cc

r_first_mean = r_first_sum / cc
r_last_mean = r_last_sum / cc
mse_mean = mse_sum / all_cc

print 'MAE in first clicked position:', r_first_mean
print 'MAE in last clicked position:', r_last_mean
print 'MSE:', mse_mean

print len(list_r_first)